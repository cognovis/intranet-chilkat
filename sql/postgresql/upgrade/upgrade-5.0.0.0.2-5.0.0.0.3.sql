-- 
-- packages/intranet-chilkat/sql/postgresql/upgrade/upgrade-5.0.0.0.2-5.0.0.0.3.sql
-- 
-- Copyright (c) 2020, cognovís GmbH, Hamburg, Germany
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
-- 
-- @author <yourname> (<your email>)
-- @creation-date 2012-01-06
-- @cvs-id $Id$
--

SELECT acs_log__debug('/packages/intranet-chilkat/sql/postgresql/upgrade/upgrade-5.0.0.0.2-5.0.0.0.3.sql','');

-- Create a storage for e-mail systems we support
SELECT im_category_new(86500,'Local Server', 'Intranet SMTP Authorization Type');
SELECT im_category_new(86501,'Remote Server with Username/Password', 'Intranet SMTP Authorization Type');
SELECT im_category_new(86502,'Microsoft 365 Oauth2', 'Intranet SMTP Authorization Type');

create or replace function inline_0 ()
returns integer as $body$
declare
        v_count                 integer;
begin
    SELECT count(*) into v_count
       FROM acs_object_type_tables
       WHERE object_type = 'person' and table_name = 'users_email';
    if v_count = 0 then
        insert into acs_object_type_tables (object_type,table_name,id_column) values ('person','users_email','user_id');
        create table users_email (
    user_id integer
        constraint users_email_pk
        primary key
        constraint users_email_pk_fk
        references users,
    smtp_auth_type_id integer constraint users_email_smtp_fk references im_categories default 86500,
    smtpuser text,
    smtppassword text,
    smtphost text,
    smtpport integer,
    imaphost text,
    imapport integer,
    imapuser text,
    imappassword text,
    imapsentfolder text
);
    end if;
    return 0;
end;$body$ language 'plpgsql';
select inline_0 ();
drop function inline_0 ();    


-- Setup initial user import
insert into users_email select user_id from users where user_id not in (select user_id from users_email);

-- Dynfields for the above
SELECT im_dynfield_widget__new (
    null,                   -- widget_id
    'im_dynfield_widget',   -- object_type
    now(),                  -- creation_date
    null,                   -- creation_user
    null,                   -- creation_ip
    null,                   -- context_id
    'category_smtp_auth_type',                -- widget_name
    'SMTP Authorization Type',  -- pretty_name
    'SMTP Authorization Types',  -- pretty_plural
    10007,                  -- storage_type_id
    'integer',              -- acs_datatype
    'im_category_tree',             -- widget
    'integer',              -- sql_datatype
    '{custom {category_type "Intranet SMTP Authorization Type" include_empty_p 0}}',
    'im_name_from_id'
);


select im_dynfield_attribute_new (
    'person',                        -- object_type
    'smtp_auth_type_id',                   -- column_name
    'SMTP Authorization Type',       -- pretty_name
    'category_smtp_auth_type',                     -- widget_name
    'integer',                            -- acs_datatype
    't',                                 -- required_p
    101,                                   -- pos y
    'f',                                 -- also_hard_coded
    'users_email'                        -- table_name
);

select im_dynfield_attribute_new (
    'person',                        -- object_type
    'smtpuser',                   -- column_name
    'SMTP Username',       -- pretty_name
    'textbox_large',                     -- widget_name
    'text',                            -- acs_datatype
    'f',                                 -- required_p
    102,                                   -- pos y
    'f',                                 -- also_hard_coded
    'users_email'                        -- table_name
);

select im_dynfield_attribute_new (
    'person',                        -- object_type
    'smtppassword',                   -- column_name
    'SMTP Password',       -- pretty_name
    'textbox_large',                     -- widget_name
    'text',                            -- acs_datatype
    'f',                                 -- required_p
    103,                                   -- pos y
    'f',                                 -- also_hard_coded
    'users_email'                        -- table_name
);

select im_dynfield_attribute_new (
    'person',                        -- object_type
    'smtphost',                   -- column_name
    'SMTP Host',       -- pretty_name
    'textbox_large',                     -- widget_name
    'text',                            -- acs_datatype
    'f',                                 -- required_p
    104,                                   -- pos y
    'f',                                 -- also_hard_coded
    'users_email'                        -- table_name
);

select im_dynfield_attribute_new (
    'person',                        -- object_type
    'smtpport',                   -- column_name
    'SMTP Port',       -- pretty_name
    'textbox_small',                     -- widget_name
    'integer',                            -- acs_datatype
    'f',                                 -- required_p
    105,                                   -- pos y
    'f',                                 -- also_hard_coded
    'users_email'                        -- table_name
);


select im_dynfield_attribute_new (
    'person',                        -- object_type
    'imapuser',                   -- column_name
    'imap Username',       -- pretty_name
    'textbox_large',                     -- widget_name
    'text',                            -- acs_datatype
    'f',                                 -- required_p
    102,                                   -- pos y
    'f',                                 -- also_hard_coded
    'users_email'                        -- table_name
);

select im_dynfield_attribute_new (
    'person',                        -- object_type
    'imappassword',                   -- column_name
    'imap Password',       -- pretty_name
    'textbox_large',                     -- widget_name
    'text',                            -- acs_datatype
    'f',                                 -- required_p
    103,                                   -- pos y
    'f',                                 -- also_hard_coded
    'users_email'                        -- table_name
);

select im_dynfield_attribute_new (
    'person',                        -- object_type
    'imaphost',                   -- column_name
    'imap Host',       -- pretty_name
    'textbox_large',                     -- widget_name
    'text',                            -- acs_datatype
    'f',                                 -- required_p
    104,                                   -- pos y
    'f',                                 -- also_hard_coded
    'users_email'                        -- table_name
);

select im_dynfield_attribute_new (
    'person',                        -- object_type
    'imapport',                   -- column_name
    'imap Port',       -- pretty_name
    'textbox_small',                     -- widget_name
    'integer',                            -- acs_datatype
    'f',                                 -- required_p
    105,                                   -- pos y
    'f',                                 -- also_hard_coded
    'users_email'                        -- table_name
);

select im_dynfield_attribute_new (
    'person',                        -- object_type
    'imapsentfolder',                   -- column_name
    'Imap Sent FOlder',       -- pretty_name
    'textbox_small',                     -- widget_name
    'integer',                            -- acs_datatype
    'f',                                 -- required_p
    105,                                   -- pos y
    'f',                                 -- also_hard_coded
    'users_email'                        -- table_name
);