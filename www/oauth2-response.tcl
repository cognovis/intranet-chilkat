ad_page_contract {
    oauth2 Reponse file which should recieve the return from the oauth2 provider for further processing
} {
    code
    state
} 

# Get the user_id from the state, which actually is the api_token
set header_token $state

set basic_api_key [base64::decode $header_token]
set user_id ""

if {[regexp {^([^\:]+)\:(.*)$} $basic_api_key match api_user_id api_token]} {
    if {$api_token ne "" && $api_user_id ne ""} {
	set valid_p [im_valid_auto_login_p -user_id $api_user_id -auto_login $api_token -check_user_requires_manual_login_p 0]
	if {$valid_p} {
	    # It is a valid token
	    set user_id $api_user_id
	}
    }
}


if {$user_id ne ""} {
    intranet_chilkat::office365::parse_tenant_consent -code $code -user_id $user_id
} 

set redirect_url [parameter::get_from_package_key -package_key intranet-chilkat -parameter "Office365RedirectUrl"]

ad_returnredirect $redirect_url

