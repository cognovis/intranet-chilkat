# packages/intranet-chilkat/tcl/intranet-chilkat-procs.tcl

## Copyright (c) 2011, cognovís GmbH, Hamburg, Germany
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see
# <http://www.gnu.org/licenses/>.
# 

ad_library {

    Initialization for intranet-chilkat

}

set bitsize [exec getconf LONG_BIT]
if {$bitsize == 32} {
    load [acs_package_root_dir intranet-chilkat]/chilkat.so
    ns_log Notice "$bitsize bit system, loading chilkat.so "
} else {
    load [acs_package_root_dir intranet-chilkat]/chilkat64.so
    ns_log Notice "$bitsize bit system, loading chilkat64.so"
}

# Unlock
set glob [new_CkGlobal]
set success [CkGlobal_UnlockBundle $glob [parameter::get_from_package_key -package_key intranet-chilkat -parameter "ChilkatLicense"]]
