# Chilkat

ad_library {
	Service procedures to work with chilkat

	@author malte.sussdorff@cognovis.de
}

namespace eval intranet_chilkat {}

ad_proc -public intranet_chilkat::ssh_exec {
	-chilkat_ssh
	-command
} {
	Execute a command over an SSH connection

	@return String send back from the SSH connection
} {

	set channelNum [CkSsh_OpenSessionChannel $chilkat_ssh]
	ns_log Notice "SendReq [CkSsh_SendReqExec $chilkat_ssh $channelNum $command]"
	
	ns_log Notice "Receivetoclose [CkSsh_ChannelReceiveToClose $chilkat_ssh $channelNum]"                
	set received_text "[CkSsh_getReceivedText $chilkat_ssh $channelNum "ansi"]"
	ds_comment "$received_text"
	return $received_text
}

ad_proc -public intranet_chilkat::ssh_connect {
	-server
	-user
	{-port ""}
	-private_key
} {
	Check if the SSH Connection works. Return 0 if it doesn't, otherwise return the SSH Channel
} {
	set sshkey [new_CkSshKey]
	
	#  First, use the LoadText convenience method to load a text file (of any SSH key format)
	#  into a string:
	set strKey [CkSshKey_loadText $sshkey $private_key]
	
	#  Next, load the key into the SSH key object.
	set success [CkSshKey_FromOpenSshPrivateKey $sshkey $strKey]

	if {[expr $success != 1]} then {
		ns_log Error "[CkSshKey_lastErrorText $sshkey]"
		return 0
	} else {
		ns_log Debug "Loaded OpenSSH private key."
	}
	
	set ssh [new_CkSsh]
	set success [CkSsh_UnlockComponent $ssh [parameter::get_from_package_key -package_key intranet-chilkat -parameter "ChilkatLicense"]]
	
	if {[expr $success != 1]} then {
		ns_log Error "INVALID LICENSE:: [CkSshKey_lastErrorText $sshkey]"
		return 0
	}
	
	#  Set some timeouts, in milliseconds:
	CkSsh_put_ConnectTimeoutMs $ssh 5000
	CkSsh_put_IdleTimeoutMs $ssh 120000
	
	set success [CkSsh_Connect $ssh $server $port]
	if {[expr $success != 1]} then {
		ns_log Error "Can't connect to server:: [CkSshKey_lastErrorText $sshkey]"
		return 0
	}
	set success [CkSsh_AuthenticatePk $ssh $user $sshkey]
	if {[expr $success != 1]} then {
		ns_log Error "Can't authenticate:: [CkSshKey_lastErrorText $sshkey]"
		return 0
	}
	ds_comment "Successful connection : $ssh"
	delete_CkSshKey $sshkey

	return $ssh
} 


# ---------------------------------------------------------------
# ZIP Functions
# ---------------------------------------------------------------
ad_proc -public intranet_chilkat::create_zip {
	{-filesystem_files ""}
	{-directories ""}
	{-extension ""}
	-zipfile
} {
	Zips the files in filesystem_files into the zip_file

	@param filesystem_files List of files with full path which are to be zipped
	@param zipfile Filepath of the zipfile
	@param directories Directories to zip up
	@param extension If directories is provided, only zip up the files matching the extension ala *\$extension
	@return zipfile path if successful, empty string if not
} {
	set zip [new_CkZip]
	set success [CkZip_UnlockComponent $zip [parameter::get_from_package_key -package_key intranet-chilkat -parameter "ChilkatLicense"]]

	if {[expr $success != 1]} then {
		ns_log Error [CkZip_lastErrorText $zip]
		delete_CkZip $zip
		return ""
		exit
	}

	#  The NewZip method only initializes the Zip object -- it does
	#  not create or write a .zip file.
	set success [CkZip_NewZip $zip "$zipfile"]
	if {[expr $success != 1]} then {
		ns_log Error "[CkZip_lastErrorText $zip]"
		delete_CkZip $zip
		return ""
		exit
	}

	#  Add a reference to a file.  This is the file that will
	#  be added to a pre-existing .zip archive.
	#  Note: this does not read or compress the file contents --
	#  it simply adds a reference to the zip object.

	set saveExtraPath 0
	foreach file $filesystem_files {
		set success [CkZip_AppendOneFileOrDir $zip $file $saveExtraPath]
		if {[expr $success != 1]} then {
			ns_log Error "[CkZip_lastErrorText $zip]"
			delete_CkZip $zip
			return ""
			exit
		}
	}

	set recurse 1
	foreach dir $directories {
		if {$extension eq ""} {
			set success [CkZip_AppendFiles $zip "$dir/*" $recurse]
		} else {
			set success [CkZip_AppendFiles $zip "$dir/*$extension" 0]
		}

		if {[expr $success != 1]} then {
			ns_log Error [CkZip_lastErrorText $zip]
			delete_CkZip $zip
			return ""
			exit
		}
	}

	set success [CkZip_WriteZipAndClose $zip]
	if {[expr $success != 1]} then {
		ns_log Error "[CkZip_lastErrorText $zip]"
		delete_CkZip $zip
		return ""
		exit
	}
	
	return "$zipfile"
	delete_CkZip $zip
}

ad_proc -public intranet_chilkat::unzip_extension {
	-zipfile
	-extension
} {
	Unzips the files in zipfile of the extension. Ignore paths

	@param zipfile Filepath of the zipfile
	@return list of unzip files, empty string if not
} {
	set zip [new_CkZip]
	set success [CkZip_UnlockComponent $zip [parameter::get_from_package_key -package_key intranet-chilkat -parameter "ChilkatLicense"]]

	if {[expr $success != 1]} then {
		puts [CkZip_lastErrorText $zip]
		delete_CkZip $zip
		return ""
		exit
	}
	
	set success [CkZip_OpenZip $zip $zipfile]
	if {[expr $success != 1]} then {
		puts [CkZip_lastErrorText $zip]
		delete_CkZip $zip
		exit
	}
	
	#  Returns the number of files and directories unzipped.
	#  Unzips to /my_files, re-creating the directory tree
	#  from the .zip.
	
	set unzip_dir [ns_tmpnam]
	exec /bin/mkdir "$unzip_dir"
	
	set unzipCount [CkZip_UnzipMatchingInto $zip $unzip_dir "*$extension" 1]
	if {[expr $unzipCount < 0]} then {
		ns_log Error "[CkZip_lastErrorText $zip]"
	}

	return "[glob -directory $unzip_dir "*.sdltb"]"
	delete_CkZip $zip
}

ad_proc -public intranet_chilkat::unzip_files {
    -zipfile
    {-dest_path}
} {
    Unzips the files in zipfile. Ignores any control files and directory structure
    
    @param zipfile Filepath of the zipfile
    @param dest_path Folder into which to copy the files after zipping
    @return list of unzip files, empty string if not
} {
    set zip [new_CkZip]
    set success [CkZip_UnlockComponent $zip [parameter::get_from_package_key -package_key intranet-chilkat -parameter "ChilkatLicense"]]
    
    if {[expr $success != 1]} then {
	puts [CkZip_lastErrorText $zip]
	delete_CkZip $zip
	return ""
	exit
    }
    
    set success [CkZip_OpenZip $zip $zipfile]
    if {[expr $success != 1]} then {
	puts [CkZip_lastErrorText $zip]
	delete_CkZip $zip
	exit
    }
	
    #  Returns the number of files and directories unzipped.
    #  Unzips to /my_files, re-creating the directory tree
    #  from the .zip.
    
    set unzip_dir [ns_tmpnam]
    exec /bin/mkdir "$unzip_dir"
	
    set unzipCount [CkZip_UnzipInto $zip $unzip_dir]
    if {[expr $unzipCount < 0]} then {
	ns_log Error "[CkZip_lastErrorText $zip]"
    }

    # Remove unwanted files
    foreach control_file [glob -nocomplain -directory $unzip_dir -types {f c b} ".*"] {
	file delete $control_file
    }

    if {$dest_path ne ""} {
	# Move the files
	foreach unzipped_file [glob -nocomplain -directory $unzip_dir -types f "*"] {
	    file rename -force $unzipped_file $dest_path
	}
    } else {
	set dest_path $unzip_dir
    }
    
    return "[glob -directory $dest_path "*"]"
    delete_CkZip $zip
}

# ---------------------------------------------------------------
# XML Functions
# ---------------------------------------------------------------
ad_proc -public intranet_chilkat::array_to_xml {
	-xml_name
	-array_name
} {
	Converts an array into an XML object with the Tag being the array name and the array key/values the Children of the tag
	@param xml_name Name of the XML Object to work with. Will be created, should be destroyed after use
	@param array_name Name of the array in the calling environment from which to get the tag an key/values
} {
	upvar $xml_name localXml
	upvar $array_name localArray

	set localXml [new_CkXml]
	CkXml_put_Tag $localXml $array_name

	foreach {key val} [array get localArray] {
		CkXml_NewChild $localXml $key $val
	}
	CkXml_GetRoot2 $localXml
}

ad_proc -public intranet_chilkat::send_mail {
    {-to_addr ""}
    {-to_party_ids ""}
    {-from_addr ""}
    {-from_party_id ""}
	{-cc_addr ""}
    {-subject ""} 
    -body
    {-filesystem_files ""}
    {-file_ids ""}
    {-originator ""}
	{ -object_id "" }
	-fixed_sender:boolean
	-no_callback:boolean
} {
    Sends an E-Mail

	@param fixed_sender Should we use the fixed sender as defined in acs-mail-lite package
	@param no_callback Should we prohibit the execution of callbacks

	@param object_id In which context do we send the E-Mail?
} {	
	if {$fixed_sender_p} {
		# Package_id required by the callback (emmar: no idea what for)
        set mail_package_id [apm_package_id_from_key "acs-mail-lite"]
        
		# Decide which sender to use
        set fixed_sender [parameter::get -parameter "FixedSenderEmail"  -package_id $mail_package_id]

        if { $fixed_sender ne "" } {
            set from_addr $fixed_sender
			set from_party_id ""
        }
	}

    # From user
    if {$from_party_id eq ""} {
	  	set from_party_id [party::get_by_email -email $from_addr]
    }

    if {$from_party_id eq ""} {
		# We do not allow sending email for just anybody
        set from_party_id [auth::get_user_id]
    }

	set smtp_auth_type_id [db_string auth_type "select smtp_auth_type_id from users_email where user_id = :from_party_id" -default 86500]

	# Rollout support
	set delivery_mode [parameter::get -package_id [acs_mail_lite::get_package_id] -parameter EmailDeliveryMode -default default]	
	switch $delivery_mode {
		log {
			set smtp_auth_type_id 86503 ; # log
			set notice "logging email instead of sending"
		}
		filter {
			set send_mode "smtp"
			set allowed_addr [parameter::get -package_id [acs_mail_lite::get_package_id] -parameter EmailAllow]
			set to_party_addr [list]
			foreach party_id $to_party_ids {
				lappend to_party_addr [party::email -party_id $party_id]
			}
			foreach recipient [concat $to_party_addr $to_addr $cc_addr] {

				# if any of the recipient is not in the allowed list
				# email message has to be sent to the log instead

				if {$recipient ni $allowed_addr} {
					set smtp_auth_type_id 86503 ; # log
					set notice "logging email because one of the recipient ($recipient) is not in the EmailAllow list"
					break
				}
			}

		}
		redirect {
			# Since we have to redirect to a list of addresses
			# we need to remove the CC and BCC ones

			set to_addr [parameter::get -package_id [acs_mail_lite::get_package_id] -parameter EmailRedirectTo]
			set to_party_ids ""
			set cc_addr ""
		}
		default {
			# proceed
		}
	}

	switch $smtp_auth_type_id {
		86500 {
			set mail_package_id [apm_package_id_from_key "acs-mail-lite"]
			
			# Get the SMTP Parameters
			set smtphost [parameter::get -parameter "SMTPHost"  -package_id $mail_package_id  -default [ns_config ns/parameters mailhost]]
			if {$smtphost eq ""} {
				set smtphost localhost
			}
			
			set smtpport [parameter::get -parameter "SMTPPort"  -package_id $mail_package_id  -default 25]
			set smtpuser [parameter::get -parameter "SMTPUser"  -package_id $mail_package_id]
			set smtppassword [parameter::get -parameter "SMTPPassword"  -package_id $mail_package_id]

			set message_id [intranet_chilkat::smtp \
				-smtphost $smtphost -smtpport $smtpport -smtpuser $smtpuser -smtppassword $smtppassword \
				-from_party_id $from_party_id -to_addr $to_addr -to_party_ids $to_party_ids -cc_addr $cc_addr \
				-subject $subject -body $body -filesystem_files $filesystem_files -file_ids $file_ids]

		}
		86501 {
			if {[db_0or1row user_smtp "select smtphost, smtpport, smtpuser, smtppassword from users_email where user_id = :from_party_id"]} {
				set message_id [intranet_chilkat::smtp \
					-smtphost $smtphost -smtpport $smtpport -smtpuser $smtpuser -smtppassword $smtppassword \
					-from_party_id $from_party_id -to_addr $to_addr -to_party_ids $to_party_ids -cc_addr $cc_addr \
					-subject $subject -body $body -filesystem_files $filesystem_files -file_ids $file_ids]
			}
		}
		86502 {
			set message_id [intranet_chilkat::office365::send_mail -from_party_id $from_party_id \
			 	-to_addr $to_addr -to_party_id $to_party_ids -cc_addr $cc_addr \
				-subject $subject -body $body -file_ids $file_ids]
		}
		86503 {
			ns_log Notice "$notice"
			set message_id "ns_log"
			ns_log Notice "intranet-chilkat::send: $subject - send from [im_name_from_id $from_party_id] ($from_party_id) to partyies $to_party_ids and to_addr $to_addr with CC $cc_addr and files $file_ids $body "
		}
	}

	if { !$no_callback_p } {
		set package_id [apm_package_id_from_key "acs-mail-lite"]
        callback acs_mail_lite::send  -package_id $package_id  -message_id $message_id  -from_addr $from_addr  -to_addr $to_addr  -body $body  -mime_type "text/html"  -subject $subject  -cc_addr $cc_addr -bcc_addr "" -file_ids $file_ids  -filesystem_files $filesystem_files  -delete_filesystem_files_p 0  -object_id $object_id
    }

	return $message_id

}

ad_proc -public intranet_chilkat::smtp {
	-smtphost:required
	-smtpport:required
	-smtpuser:required
	-smtppassword:required
	-from_party_id:required
	{ -to_addr "" }
	{ -cc_addr "" }
	{ -to_party_ids "" }
 	{-subject ""} 
    -body
    {-filesystem_files ""}
    {-file_ids ""}
} {
	Send E-mail using SMTP

	@param smtphost Host which will handle mail sending
	@param smtpport Port on which the host works (typicall 465 for SSL and 25 for normal non smtp
	@param smtpuser Username with which to authenticate
	@param smtppassword Password to authenticate (if required
	@param from_party_id UserID used for sending this mail
	@param to_party_ids UserIDs whom we send the mail to
	@param to_addr To Addresses for users where we don't have the party_ids
	@param subject Subject of the mail
	@param body HTML body for the email
} {
	set err_msg ""

	# The mailman object is used for sending and receiving email.
	set mailman [new_CkMailMan]
	# Set the SMTP server.
	CkMailMan_put_SmtpHost $mailman $smtphost
	
	# Set SMTP login and password (if necessary)
	CkMailMan_put_SmtpUsername $mailman $smtpuser
	CkMailMan_put_SmtpPassword $mailman $smtppassword

	switch $smtpport {
		465 {
			CkMailMan_put_SmtpSsl $mailman 1
		}
		25 {
			CkMailMan_put_SmtpSsl $mailman 0
		}
	}
		
	CkMailMan_put_SmtpPort $mailman $smtpport
	
	# Create a new email object
	set email [new_CkEmail]

	if {[ad_looks_like_html_p $body]} {
		set plain_body [ad_html_to_text $body]
	} else {
		set plain_body $body
		set body [ad_convert_to_html $body]
	}

	CkEmail_AddPlainTextAlternativeBody $email $plain_body 
	CkEmail_AddHtmlAlternativeBody $email $body

	CkEmail_put_Subject $email [acs_mail_lite::utils::build_subject $subject]
	
	# From user
	set from_addr [party::email -party_id $from_party_id]
	set name [acs_mail_lite::utils::build_subject [person::name -person_id $from_party_id]]
	CkEmail_put_From $email "$name <$from_addr>"

	# To user
	foreach to_party_id $to_party_ids {
		set to_addr [party::email -party_id $to_party_id]
		set name [acs_mail_lite::utils::build_subject [person::name -person_id $to_party_id]]
		CkEmail_AddTo $email $name $to_addr
	}

	foreach to_addr $to_addr {
		set name  [acs_mail_lite::utils::build_subject [person::name -email $to_addr]]
		if {$name eq ""} {set name $to_addr}
		CkEmail_AddTo $email $name $to_addr
	}

	foreach cc_addr $cc_addr {
		set name  [acs_mail_lite::utils::build_subject [person::name -email $cc_addr]]
		if {$name eq ""} {set name $cc_addr}
		CkEmail_AddCC $email $name $cc_addr
	}

	# ...from file-storage
	if {$file_ids ne ""} {
		set revision_ids [list]
		
		# Check if we are dealing with revisions or items.
		foreach file_id $file_ids {
			set item_id [content::revision::item_id -revision_id $file_id]
			if {$item_id ne ""} {
				lappend revision_ids $file_id
			} else {
				lappend revision_ids [content::item::get_latest_revision -item_id $file_id]
			}
		}

		foreach revision_id $revision_ids {
			if {[db_0or1row file_info "select mime_type, title, content, storage_area_key from cr_revisions r, cr_items i where r.revision_id = :revision_id and i.item_id = r.item_id"]} {
				if {$content ne ""} {	
					set file_path [cr_fs_path $storage_area_key]${content}

					# Can't use CkEmail_AddFileAttachment as we have to specify the filename
					set fac [new_CkFileAccess]
					set fileBytes [new_CkByteData]
					set success [CkFileAccess_ReadEntireFile $fac $file_path $fileBytes]

					if {$success != 1} then {
						set err_msg [CkEmail_lastErrorText $fac]
						lappend err_msg_list $err_msg
						ns_log Error "Could not read $file_path for $title ... $err_msg"
						continue
					}
					
					set success [CkEmail_AddDataAttachment2 $email "$title" $fileBytes "$mime_type"]
					
					if {$success != 1} then {
						set err_msg "Could not attach the file: \n \n [CkEmail_lastErrorText $email]"
						ns_log Error "$err_msg"
						# Still allow sending the mail.. maybe not smart?
						continue
					}
					delete_CkFileAccess $fac
					delete_CkByteData $fileBytes
				}
			}
		}
	}

	set success [CkMailMan_SendEmail $mailman $email]
	if {[expr $success != 1]} then {
		set err_msg "Could not send the email: \n \n[CkMailMan_lastErrorText $mailman]"
		ns_log Error $err_msg
	} 
	
	set success_close [CkMailMan_CloseSmtpConnection $mailman]
	if {[expr $success_close != 1]} then {
		ns_log Error "Connection to SMTP server not closed cleanly."
	}
	
	# The mail has been sent.  Now save the email to the "Sent" folder
	# on your mail server.  (Your "Sent" folder could be named something else,
	# or you may store the email in any folder of your choosing.)
	if {[db_0or1row user_smtp "select imaphost, imapport, imapuser, imappassword, imapsentfolder from users_email where user_id = :from_party_id"]} {
		if {$imaphost ne ""} {

			if {$imapport eq ""} {
				set imapport 993
			}

			if {$imapuser eq ""} {
				set imapuser $smtpuser
			}

			if {$imappassword eq ""} {
				set imappassword $smtppassword
			}

			if {$imapsentfolder eq ""} {
				set imapsentfolder "Sent"
			}

			set imap [new_CkImap]

			# Connect to an IMAP server.
			# Use TLS
			CkImap_put_Ssl $imap 1
			CkImap_put_Port $imap $imapport
			if {$success eq 1} {
				set success [CkImap_Connect $imap $imaphost]
				if {$success != 1} then {
					set err_msg "Can't connect to smtp server [CkImap_lastErrorText $imap]"
					ns_log Error $err_msg
				}
			}

			# Login
			if {$success eq 1} {
				set success [CkImap_Login $imap $imapuser $imappassword]
				if {$success != 1} then {
					set err_msg "Can't login to server $imaphost - [CkImap_lastErrorText $imap]"
					ns_log Error $err_msg
				}
			}

			if {$success eq 1} {
				# Upload (save) the email to the "Sent" mailbox.
				set success [CkImap_AppendMail $imap "Sent" $email]
				if {$success != 1} then {
					set err_msg "Can't move the mail to the send folder [CkImap_lastErrorText $imap]"
					ns_log Error $err_msg
				}
			}

			# Disconnect from the IMAP server.
			set success [CkImap_Disconnect $imap]
			delete_CkImap $imap	
		}
	}

	set message_id [CkEmail_computeGlobalKey2 $email "base64" 0]

	delete_CkMailMan $mailman
	delete_CkEmail $email
	if {$err_msg eq ""} {
		return $message_id
	}
}

ad_proc -private -deprecated intranet_chilkat::smtp_old {
    -multi_token:required
    -headers:required
    -originator:required
    {-filesystem_files ""}
    {-file_ids ""}
} {
    Sends an E-Mail using SMTP with SSL TLS etc.
} {
    set mail_package_id [apm_package_id_from_key "acs-mail-lite"]

    # Get the SMTP Parameters
    set smtpHost [parameter::get -parameter "SMTPHost"  -package_id $mail_package_id  -default [ns_config ns/parameters mailhost]]
    if {$smtpHost eq ""} {
	set smtpHost localhost
    }
    
    set smtpport [parameter::get -parameter "SMTPPort"  -package_id $mail_package_id  -default 25]
    set smtpuser [parameter::get -parameter "SMTPUser"  -package_id $mail_package_id]
    set smtppassword [parameter::get -parameter "SMTPPassword"  -package_id $mail_package_id]

    # The mailman object is used for sending and receiving email.
    set mailman [new_CkMailMan]
    
    # Set the SMTP server.
    CkMailMan_put_SmtpHost $mailman $smtpHost
    
    # Set SMTP login and password (if necessary)
    CkMailMan_put_SmtpUsername $mailman $smtpuser
    CkMailMan_put_SmtpPassword $mailman $smtppassword

    switch $smtpport {
	465 {
	    CkMailMan_put_SmtpSsl $mailman 1
	}
	25 {
	    CkMailMan_put_SmtpSsl $mailman 0
	}
    }
    	
    CkMailMan_put_SmtpPort $mailman $smtpport
    
    # Create a new email object
    set email [new_CkEmail]

    # Add Originator for bouncing and verification
    CkEmail_put_Sender $email $originator
    CkEmail_put_BounceAddress $email $originator    

    foreach header $headers {
	set key [lindex $header 0]
	set value [lindex $header 1]
	switch $key {
	    From {
		set name [acs_mail_lite::utils::build_subject [person::name -email $value]]
		if {$name eq ""} {set name $value}
		CkEmail_put_From $email "$name <$value>"
	    }
	    Reply-To {
		set name [acs_mail_lite::utils::build_subject [person::name -email $value]]
		if {$name eq ""} {set name $value}
		CkEmail_put_ReplyTo $email "$name <$value>"
	    }
	    To {
		set to_addr_list [split $value ","]
		foreach to_addr $to_addr_list {
		    set name  [acs_mail_lite::utils::build_subject [person::name -email $to_addr]]
		    if {$name eq ""} {set name $to_addr}
		    CkEmail_AddTo $email $name $to_addr
		}
	    }
	    CC {
		set cc_addr_list [split $value ","]
		foreach cc_addr $cc_addr_list {
		    set name  [acs_mail_lite::utils::build_subject [person::name -email $cc_addr]]
		    if {$name eq ""} {set name $cc_addr}
		    CkEmail_AddCC $email $name $cc_addr
		}
	    }
	    DCC - BCC {
		set cc_addr_list [split $value ","]
		foreach cc_addr $cc_addr_list {
		    set name  [acs_mail_lite::utils::build_subject [person::name -email $cc_addr]]
		    if {$name eq ""} {set name $cc_addr}
		    CkEmail_AddBcc $email $name $cc_addr
		}
	    }
	    default {
		ns_log Notice "intranet-chilkat::smtp unknown key  $key with value $value"
	    }
	}
    }

    # Default set parts so the variable is set in case the
    # mime has no parts
    set parts "" 
    foreach {key value}  [::mime::getproperty $multi_token] {
	switch $key {
	    params {
	    }
	    parts {
		# We have multiple parts
		set parts $value
	    }
	    size {
	    }
	    encoding {
	    }
	    content {
	    }
	}
    }

    # If we have parts, loop through them to get the body
    foreach part $parts {
	foreach {key value} [::mime::getproperty $part] {
	    set $key $value
	}

	# Encode the body
	# Evil hack as I have no other idea what to do
	set body [encoding convertfrom [ns_encodingforcharset "UTF-8"] [::mime::getbody $part]]

	CkEmail_put_Utf8 $email 1
	
	switch $content {
	    "text/plain" {
		CkEmail_AddPlainTextAlternativeBody $email [ad_html_to_text $body]
	    }
	    "text/html" {
		CkEmail_AddHtmlAlternativeBody $email $body
	    }
	}
    }

    if {$parts eq ""} {
	# Plain Text mail
	CkEmail_put_Body $email "[::mime::getbody $multi_token]"
    }
    
    foreach {key value}  [::mime::getheader $multi_token] {
	switch $key {
	    date {
		CkEmail_put_EmailDateStr $email $value
	    }
	    Subject {
		CkEmail_put_Subject $email $value
	    }
	    default {
		ns_log Notice "intranet-chilkat::smtp unknown header $key with value $value"
	    }
	}
    }
    
    set success [CkMailMan_SendEmail $mailman $email]
    if {[expr $success != 1]} then {
	ns_log Error [CkMailMan_lastErrorText $mailman]
    } 
    
    set success [CkMailMan_CloseSmtpConnection $mailman]
    if {[expr $success != 1]} then {
	ns_log Error "Connection to SMTP server not closed cleanly."
    }
    
    delete_CkMailMan $mailman
    delete_CkEmail $email
}
