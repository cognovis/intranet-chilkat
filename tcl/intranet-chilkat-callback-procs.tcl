ad_proc -public -callback person_after_create -impl chilkat_email {
    -object_id:required
    { -status_id ""}
    { -type_id ""}
} {
    Add the user to the users_office365 table if they have the data enabled
} { 
    db_dml insert_user "insert into users_email (user_id) values (:object_id)"
}

ad_proc -public -callback person_before_update -impl chilkat_email {
    -object_id:required
    { -status_id ""}
    { -type_id ""}
} {
    Add the user to the users_office365 table if not already added
} { 
    set users_email_p [db_string users_email_p "select 1 from users_email where user_id = :object_id" -default 0
    if {!$users_email_p} {
        db_dml insert_user "insert into users_email (user_id) values (:object_id)"
    }
}