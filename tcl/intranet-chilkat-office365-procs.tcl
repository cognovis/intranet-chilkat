
ad_library {
	Service procedures to work with chilkat and office 365

	@author malte.sussdorff@cognovis.de
}

namespace eval intranet_chilkat::office365 {
    
    ad_proc -public oauth2_access_url {

    } {
        Register the token?
    } {
        set client_secret [parameter::get_from_package_key -package_key intranet-chilkat -parameter "Office365ClientSecret"]
        set client_id [parameter::get_from_package_key -package_key intranet-chilkat -parameter "Office365ClientId"]
        set authorization_endpoint [parameter::get_from_package_key -package_key intranet-chilkat -parameter "Office365OauthAuthorizationEndpoint"]

        set user_id [auth::require_login]

        set auto_login [im_generate_auto_login -user_id $user_id]
        set api_key [base64::encode "${user_id}:$auto_login"]

        set sbUrl [new_CkStringBuilder]

        CkStringBuilder_Append $sbUrl "${authorization_endpoint}?"
        CkStringBuilder_Append $sbUrl "response_type=code&"
        CkStringBuilder_Append $sbUrl "state=${api_key}&"
        CkStringBuilder_Append $sbUrl "client_id=${client_id}&"

        # Provide a SPACE separated list of scopes.
        # Important: The offline_access scope is needed to get a refresh token.
        set sbScope [new_CkStringBuilder]
        set scope [parameter::get_from_package_key -package_key intranet-chilkat -parameter "Office365Scope"]
        CkStringBuilder_Append $sbScope $scope
        CkStringBuilder_Append $sbUrl "scope="
        CkStringBuilder_Append $sbUrl "[CkStringBuilder_getEncoded $sbScope "url" "utf-8"]&"

        set sbRedirectUri [new_CkStringBuilder]
        set redirect_uri [parameter::get_from_package_key -package_key intranet-chilkat -parameter "Office365RedirectUri"]
        if {$redirect_uri eq ""} {
            set redirect_uri "[ad_url]/intranet-chilkat/oauth2-response"            
        } else {
            set redirect_uri "${redirect_uri}/intranet-chilkat/oauth2-response"
        }        
        CkStringBuilder_Append $sbRedirectUri $redirect_uri
        CkStringBuilder_Append $sbUrl "redirect_uri="
        CkStringBuilder_Append $sbUrl [CkStringBuilder_getEncoded $sbRedirectUri "url" "utf-8"]        

        return [CkStringBuilder_getAsString $sbUrl]
    }

    ad_proc parse_tenant_consent {
        -code:required
        -user_id:required
    } {
        Parses the tenant parse_tenant_consent

        @param code string String to use to call back to microsoft
    } {
        set token_endpoint [parameter::get_from_package_key -package_key intranet-chilkat -parameter "Office365OauthTokenEndpoint"]
        set client_secret [parameter::get_from_package_key -package_key intranet-chilkat -parameter "Office365ClientSecret"]
        set client_id [parameter::get_from_package_key -package_key intranet-chilkat -parameter "Office365ClientId"]

        set http [new_CkHttp]
        set req [new_CkHttpRequest]
        CkHttpRequest_put_HttpVerb $req "POST"
        CkHttpRequest_put_Path $req $token_endpoint
        
        set redirect_uri [parameter::get_from_package_key -package_key intranet-chilkat -parameter "Office365RedirectUri"]
        if {$redirect_uri eq ""} {
            set redirect_uri "[ad_url]/intranet-chilkat/oauth2-response"            
        } else {
            set redirect_uri "${redirect_uri}/intranet-chilkat/oauth2-response"
        }        

        CkHttpRequest_AddParam $req "client_id" "$client_id"        
        CkHttpRequest_AddParam $req "redirect_uri" "$redirect_uri"
        CkHttpRequest_AddParam $req "client_secret" "$client_secret"        
        CkHttpRequest_AddParam $req "grant_type" "authorization_code"
        CkHttpRequest_AddParam $req "code" "$code"

        set resp [CkHttp_PostUrlEncoded $http $token_endpoint $req]
        if {[CkHttp_get_LastMethodSuccess $http] == 0} then {
            puts [CkHttp_lastErrorText $http]
            delete_CkHttp $http
            delete_CkHttpRequest $req
            exit
        }

        #ns_log Notice "Response status code = [CkHttpResponse_get_StatusCode $resp]"
        #ns_log Notice "Parse Tenant Consent Response body: [CkHttpResponse_bodyStr $resp]"


        set jsonResp [new_CkJsonObject]
        CkJsonObject_Load $jsonResp [CkHttpResponse_bodyStr $resp]

        set access_token "[CkJsonObject_stringOf $jsonResp "access_token"]"
        set refresh_token "[CkJsonObject_stringOf $jsonResp "refresh_token"]"
        set id_token "[CkJsonObject_stringOf $jsonResp "id_token"]"
        
        set update_p [db_string update_p "select 1 from users_office365 where user_id = :user_id" -default 0]
        if {$update_p} {
            # Update
            db_dml update_access_token "update users_office365 set access_token = :access_token, refresh_token=:refresh_token, id_token=:id_token where user_id = :user_id"
        } else {
            db_dml insert_token "insert into users_office365 (user_id,access_token,refresh_token,id_token) values (:user_id,:access_token,:refresh_token,:id_token)"
        }
        delete_CkHttpResponse $resp
        delete_CkHttp $http
        delete_CkHttpRequest $req
    }

     ad_proc refresh_token {
        -user_id:required
    } {
        Refresh the token for the user
    } {
        set token_endpoint [parameter::get_from_package_key -package_key intranet-chilkat -parameter "Office365OauthTokenEndpoint"]
        set client_secret [parameter::get_from_package_key -package_key intranet-chilkat -parameter "Office365ClientSecret"]
        set client_id [parameter::get_from_package_key -package_key intranet-chilkat -parameter "Office365ClientId"]
        set scope [parameter::get_from_package_key -package_key intranet-chilkat -parameter "Office365Scope"]
        set refresh_token [db_string refresh_token "select refresh_token from users_office365 where user_id = :user_id" -default ""]

        if {$refresh_token ne ""} {
            set http [new_CkHttp]
            set req [new_CkHttpRequest]
            CkHttpRequest_put_HttpVerb $req "POST"
            CkHttpRequest_put_Path $req $token_endpoint
            
            set redirect_uri [parameter::get_from_package_key -package_key intranet-chilkat -parameter "Office365RedirectUri"]
            if {$redirect_uri eq ""} {
                set redirect_uri "[ad_url]/intranet-chilkat/oauth2-response"            
            } else {
                set redirect_uri "${redirect_uri}/intranet-chilkat/oauth2-response"
            }        

            CkHttpRequest_AddParam $req "client_id" "$client_id"
            CkHttpRequest_AddParam $req "scope" "$scope"
            CkHttpRequest_AddParam $req "refresh_token" "$refresh_token"
            CkHttpRequest_AddParam $req "client_secret" "$client_secret"        
            CkHttpRequest_AddParam $req "grant_type" "refresh_token"
        
            set resp [CkHttp_PostUrlEncoded $http $token_endpoint $req]
            if {[CkHttp_get_LastMethodSuccess $http] == 0} then {
                ns_log Error [CkHttp_lastErrorText $http]
                delete_CkHttp $http
                delete_CkHttpRequest $req
                exit
            }

            set jsonResp [new_CkJsonObject]
            CkJsonObject_Load $jsonResp [CkHttpResponse_bodyStr $resp]

            set access_token "[CkJsonObject_stringOf $jsonResp "access_token"]"
            set refresh_token "[CkJsonObject_stringOf $jsonResp "refresh_token"]"
            set id_token "[CkJsonObject_stringOf $jsonResp "id_token"]"
            set update_p [db_string update_p "select 1 from users_office365 where user_id = :user_id" -default 0]
            if {$update_p} {
                # Update
                db_dml update_access_token "update users_office365 set access_token = :access_token, refresh_token=:refresh_token, id_token=:id_token where user_id = :user_id"
            } else {
                db_dml insert_token "insert into users_office365 (user_id,access_token,refresh_token,id_token) values (:user_id,:access_token,:refresh_token,:id_token)"
            }
            delete_CkHttpResponse $resp
            delete_CkHttp $http
            delete_CkHttpRequest $req
        } else {
            # Redirect the user to the authorization instead
            ad_returnredirect "/intranet-chilkat/"
        }
    }

    ad_proc -public send_mail {
        {-to_addr ""}
        {-to_party_id ""}
        {-from_addr ""}
        {-from_party_id ""}
        {-cc_addr ""}
        {-subject ""} 
        -body
        {-filesystem_files ""}
        {-file_ids ""}
        {-originator ""}
    } {
        Sends an email from the currently logged in user 

        @param subject Subject of the email
        @param body Body (might be html) of the email

        @param user_id In case we send on behalf of somebody else
    } {
        
        # From user
        if {$from_party_id eq ""} {
	    	set from_party_id [party::get_by_email -email $from_addr]
        }

        if {$from_party_id eq ""} {
            return 
        }

	set from_addr [party::email -party_id $from_party_id]

        set access_token [db_string access_token "select access_token from users_office365 where user_id = :from_party_id" -default ""]    
        if {$access_token eq ""} {
	    if {$to_addr eq ""} {
		set to_addr [party::email -party_id [lindex $to_party_id 0]]
	    }
	    ns_log Notice "Sending mail through the old interface for $from_addr sending to $to_addr"
	    acs_mail_lite::send -send_immediately -from_addr $from_addr -to_addr $to_addr -cc_addr $cc_addr \
		-subject $subject -body $body -filesystem_files $filesystem_files -file_ids $file_ids -use_sender

            # Redirect the user to the authorization instead
            ad_returnredirect "/intranet-chilkat/"
	    
	    return ""
        } else {
	    # Refresh the token
	    intranet_chilkat::office365::refresh_token -user_id $from_party_id
	    set access_token [db_string access_token "select access_token from users_office365 where user_id = :from_party_id" -default ""]    
	}


        # Create a new email object
        set email [new_CkEmail]
        
        CkEmail_put_Subject $email [acs_mail_lite::utils::build_subject $subject]
	set body [encoding convertto "iso8859-1" $body]
	if {[ad_looks_like_html_p $body]} {
		set plain_body [ad_html_to_text $body]
	} else {
	    set plain_body $body
	    set body [ad_convert_to_html $body]
	}

	set body [encoding convertfrom [ns_encodingforcharset "UTF-8"] $body]

	CkEmail_put_Utf8 $email 1

	CkEmail_AddPlainTextAlternativeBody $email $plain_body
	CkEmail_AddHtmlAlternativeBody $email $body

	set name [acs_mail_lite::utils::build_subject [person::name -person_id $from_party_id]]
        CkEmail_put_From $email "$name <$from_addr>"
        
        # To user
        if {$to_party_id eq ""} {
            set name  [person::name -email $to_addr]
            if {$name eq ""} {set name $to_addr}
        } else {
            set to_addr [party::email -party_id $to_party_id]
            set name [person::name -person_id $to_party_id]
        }   
        CkEmail_AddTo $email $name $to_addr

        foreach cc_addr $cc_addr {
            set name  [person::name -email $cc_addr]
            if {$name eq ""} {set name $cc_addr}
            CkEmail_AddCC $email $name $cc_addr
        }
 

        # ...from file-storage
        if {$file_ids ne ""} {
            set revision_ids [list]
            
            # Check if we are dealing with revisions or items.
            foreach file_id $file_ids {
                set item_id [content::revision::item_id -revision_id $file_id]
                if {$item_id ne ""} {
                    lappend revision_ids $file_id
                } else {
                    lappend revision_ids [content::item::get_latest_revision -item_id $file_id]
                }
            }

            foreach revision_id $revision_ids {
                if {[db_0or1row file_info "select mime_type, title, content, storage_area_key from cr_revisions r, cr_items i where r.revision_id = :revision_id and i.item_id = r.item_id"]} {
                    if {$content ne ""} {	
                        set file_path [cr_fs_path $storage_area_key]${content}

                        # Can't use CkEmail_AddFileAttachment as we have to specify the filename
                        set fac [new_CkFileAccess]
                        set fileBytes [new_CkByteData]
                        set success [CkFileAccess_ReadEntireFile $fac $file_path $fileBytes]

                        if {$success != 1} then {
                            ns_log Error "Could not read $file_path for $title ... [CkEmail_lastErrorText $fac]"
                        }
                        
                        set success [CkEmail_AddDataAttachment2 $email "$title" $fileBytes "$mime_type"]
                        
                        if {$success != 1} then {
                            ns_log Error "Could not attach file:  [CkEmail_lastErrorText $email]"
                            # Still allow sending the mail.. maybe not smart?
                        }
                        delete_CkFileAccess $fac
                        delete_CkByteData $fileBytes
                    }
                }
            }
        }

        set mailman [new_CkMailMan]

        CkMailMan_put_SmtpHost $mailman "smtp.office365.com"
        CkMailMan_put_SmtpPort $mailman 587
        CkMailMan_put_StartTLS $mailman 1

        # Use your Office365 email address for the SmtpUsername.
        CkMailMan_put_SmtpUsername $mailman "$from_addr"
        CkMailMan_put_OAuth2AccessToken $mailman "$access_token"

        set success [CkMailMan_SendEmail $mailman $email]
        if {[expr $success != 1]} then {
            ns_log Error "Could not send E-Mail: \n [CkMailMan_lastErrorText $mailman]"


	    acs_mail_lite::send -send_immediately -from_addr $from_addr -to_addr $to_addr -cc_addr $cc_addr \
		-subject $subject -body $body -filesystem_files $filesystem_files -file_ids $file_ids -use_sender

        } 
        
        set success [CkMailMan_CloseSmtpConnection $mailman]
        if {[expr $success != 1]} then {
            ns_log Error "Connection to SMTP server not closed cleanly."
        }

    	set message_id [CkEmail_computeGlobalKey2 $email "base64" 0]

        delete_CkMailMan $mailman
        delete_CkEmail $email
        return $message_id
    }
}
